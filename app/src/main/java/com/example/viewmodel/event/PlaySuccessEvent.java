package com.example.viewmodel.event;

import com.example.viewmodel.data.model.Repo;

import java.util.List;

/**
 * Event used when games request to server is successful
 */
public class PlaySuccessEvent {
    private List<Repo> mPlays;

    private PlaySuccessEvent(List<Repo> plays) {
        mPlays = plays;
    }

    public List<Repo> getPlays() {
        return mPlays;
    }

    public static PlaySuccessEvent playsEvent(List<Repo> plays) {
        return new PlaySuccessEvent(plays);
    }

}
