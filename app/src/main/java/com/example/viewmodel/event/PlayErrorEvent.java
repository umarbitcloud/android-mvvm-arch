package com.example.viewmodel.event;


/**
 * Event when server returns error for games request
 */
public class PlayErrorEvent {
    public static String getError() {
        return "error";
    }
}
