package com.example.viewmodel.loader;

public interface CommonLoader<T> {
    void requestData(T data);
    void clearObserver();
}
