package com.example.viewmodel.loader;

import com.example.viewmodel.data.model.Repo;
import com.example.viewmodel.data.rest.RepoService;
import com.example.viewmodel.event.PlayErrorEvent;
import com.example.viewmodel.event.PlaySuccessEvent;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PlayLoader implements CommonLoader<Void> {

    private final EventBus mEventBus;
    private final RepoService mRepoService;
    private CompositeDisposable mCompositeDisposable;


    @Inject
    public PlayLoader(@NotNull EventBus eventBus, @NotNull RepoService playService) {
        this.mEventBus = eventBus;
        this.mRepoService = playService;
        this.mCompositeDisposable = new CompositeDisposable();;
    }

    @Override
    public void requestData(Void data) {

        mCompositeDisposable.add(mRepoService.getRepositories().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<Repo>>() {
            @Override
            public void onSuccess(List<Repo> value) {
                mEventBus.post(PlaySuccessEvent.playsEvent(value));
            }

            @Override
            public void onError(Throwable e) {
                mEventBus.post(PlayErrorEvent.getError());
            }
        }));

    }

    @Override
    public void clearObserver() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

}
