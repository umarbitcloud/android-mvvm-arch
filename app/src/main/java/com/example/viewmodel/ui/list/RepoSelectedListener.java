package com.example.viewmodel.ui.list;

import com.example.viewmodel.data.model.Repo;

public interface RepoSelectedListener {

    void onRepoSelected(Repo repo);
}
