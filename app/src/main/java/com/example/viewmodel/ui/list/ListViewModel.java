package com.example.viewmodel.ui.list;

import com.example.viewmodel.data.model.Repo;
import com.example.viewmodel.event.PlayErrorEvent;
import com.example.viewmodel.event.PlaySuccessEvent;
import com.example.viewmodel.repository.MainRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

public class ListViewModel extends ViewModel {

    private final MainRepository mMainRepository;

    private EventBus mEventBus;

    private final MutableLiveData<List<Repo>> repos = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    @Inject
    public ListViewModel(MainRepository mainRepository, EventBus eventBus) {
        this.mMainRepository = mainRepository;
        mEventBus = eventBus;
        mEventBus.register(this);
        fetchRepos();
    }

    LiveData<List<Repo>> getRepos() {
        return repos;
    }

    LiveData<Boolean> getError() {
        return repoLoadError;
    }

    LiveData<Boolean> getLoading() {
        return loading;
    }

    private void fetchRepos() {
        loading.setValue(true);
        mMainRepository.getRepositories();
    }

    @Subscribe
    public void onEvent(PlaySuccessEvent event) {
        repoLoadError.setValue(false);
        repos.setValue(event.getPlays());
        loading.setValue(false);

    }

    @Subscribe
    public void onEvent(PlayErrorEvent event) {
        // To handle errors in form of banners, snackbar or dialogs
        repoLoadError.setValue(true);
        loading.setValue(false);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mMainRepository.clearPlayLoader();
        mEventBus.unregister(this);
    }
}
