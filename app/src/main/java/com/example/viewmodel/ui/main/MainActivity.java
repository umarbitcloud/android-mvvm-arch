package com.example.viewmodel.ui.main;

import com.example.viewmodel.ui.list.ListFragment;

import android.os.Bundle;

import com.example.viewmodel.R;
import com.example.viewmodel.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, new ListFragment()).commit();
    }
}
