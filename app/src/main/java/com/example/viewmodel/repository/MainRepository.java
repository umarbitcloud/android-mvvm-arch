package com.example.viewmodel.repository;

import com.example.viewmodel.data.model.Repo;
import com.example.viewmodel.data.rest.RepoService;
import com.example.viewmodel.loader.PlayLoader;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class MainRepository {
    private final PlayLoader mPlayLoader;

    @Inject
    public MainRepository(@NotNull PlayLoader playLoader) {
        this.mPlayLoader = playLoader;
    }

    /*@NotNull
    public final Single<List<Repo>> getRepositories() {
        return mRepoService.getRepositories();
    }*/

    @NotNull
    public void getRepositories() {
        mPlayLoader.requestData(null);
    }

    public void clearPlayLoader(){
        mPlayLoader.clearObserver();
    }


    @NotNull
    public Single<Repo> getRepo(String owner, String name) {
        //return mPlayLoader.getRepo(owner, name);
        return null;
    }

}
