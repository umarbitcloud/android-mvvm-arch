package com.example.viewmodel.di.component;

import com.example.viewmodel.di.module.ActivityBindingModule;
import com.example.viewmodel.di.module.ContextModule;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;
import com.example.viewmodel.base.BaseApplication;
import com.example.viewmodel.di.module.ApplicationModule;
import com.example.viewmodel.di.module.EventBusModule;

@Singleton
@Component(modules = {ContextModule.class, ApplicationModule.class, AndroidSupportInjectionModule.class, ActivityBindingModule.class,
        EventBusModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}