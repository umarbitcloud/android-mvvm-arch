package com.example.viewmodel.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.example.viewmodel.ui.main.MainActivity;
import com.example.viewmodel.ui.main.MainFragmentBindingModule;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();
}
