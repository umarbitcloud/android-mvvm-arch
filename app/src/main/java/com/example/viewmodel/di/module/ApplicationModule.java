package com.example.viewmodel.di.module;

import com.example.viewmodel.data.rest.OpenRepoApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
@Module(includes = ViewModelModule.class)
public class ApplicationModule {

    private static final String BASE_URL = "https://api.github.com/";
    private static OkHttpClient sOkHttpClient;
    private static final long CONNECTION_TIMEOUT = 15;
    private static final long READ_TIMEOUT = 15;

    @Singleton
    @Provides
    static Retrofit provideRetrofit() {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient())
                .build();
    }

    @Singleton
    @Provides
    static OpenRepoApi provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(OpenRepoApi.class);
    }

    private static OkHttpClient okHttpClient() {
        if (sOkHttpClient == null) {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true);

            sOkHttpClient = builder.build();

        }
        return sOkHttpClient;
    }
}
