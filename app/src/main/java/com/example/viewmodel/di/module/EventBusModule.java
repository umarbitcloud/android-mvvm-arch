package com.example.viewmodel.di.module;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class EventBusModule {
    private static EventBus sEventBus = EventBus.getDefault();

    @Singleton
    @Provides
    public static EventBus eventBus() {
        return sEventBus;
    }
}
