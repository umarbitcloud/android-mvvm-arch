package com.example.viewmodel.data.rest;

import com.example.viewmodel.data.model.Repo;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RepoService {

    private final OpenRepoApi mOpenRepoApi;

    @Inject
    public RepoService(OpenRepoApi mOpenRepoApi) {
        this.mOpenRepoApi = mOpenRepoApi;
    }

    public Single<List<Repo>> getRepositories() {
        return mOpenRepoApi.getRepositories();
    }

    public Single<Repo> getRepo(String owner, String name) {
        return mOpenRepoApi.getRepo(owner, name);
    }
}
